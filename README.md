# Node.js – Store
Store is an in-memory data storage, used as a cache database

[![Build Status](https://travis-ci.org/bart96-b/store.svg)](https://travis-ci.org/bart96-b/store)

## Installation
```bash
$ npm install @bart96/store
```

## Usage
```js
const store = require('@bart96/store');

// String
store.set('name', 'Victor');
store.get('name'); // Victor

// Array
store.set('fruits', ['apple', 'banana', 'pear']);
store.get('fruits'); //

// Object
store.set('user', {age:25, gender:'male'});
store.get('user'); // {age:25, gender:'male'}

// Function
store.set('now', Date.now);
store.get('now')(); // 1578510628070

// Multi keys
store.set('shop:cars:folkswagen:colors', ['white', 'black']);
store.get('shop:cars'); // {folkswagen: {colors: ['white', 'black']}}
```

## Future
```js
const store = require('@bart96/store');

// TTL
store.set('name', 'Victor', 5000);
store.get('name'); // Victor
setTimeout(() => store.get('name'), 5000); // undefined

// Functions
store.each((key, value) => {...}); // {name:'Victor', fruits:['apple','banana','pear'], user:{age:25, gender:'male'}, now:Function}
store.edit((key, value) => typeof value === 'object'); // {fruits:['apple','banana','pear'], user:{age:25, gender:'male'}}

// Namespaces
store.create('App1', isPrivate); // Create new if not exists
store.delete('App1');

// Error handlers
// Yes, they are absent :D

// And more sweets :)
```
