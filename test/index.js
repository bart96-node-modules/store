const assert = require('assert');
const store = require('..');

describe('String', () => {
	it('#1', () => assert.equal(store.set('name', 'Victor'), 'Victor'));
	it('#2', () => assert.equal(store.get('name'), 'Victor'));
});

describe('Array', () => {
	it('#1', () => assert.deepEqual(store.set('fruits', ['apple', 'banana']), ['apple', 'banana']));
	it('#2', () => assert.deepEqual(store.get('fruits'), ['apple', 'banana']));
	it('#3', () => {
    store.get('fruits').push('pear');
    return assert.deepEqual(store.get('fruits'), ['apple', 'banana', 'pear']);
  });
});

describe('Object', () => {
	it('#1', () => assert.deepEqual(store.set('user', {age:25, gender:'male'}), {age:25, gender:'male'}));
	it('#2', () => assert.deepEqual(store.get('user'), {age:25, gender:'male'}));
});

describe('Function', () => {
	it('#1', () => assert.equal(store.set('now', Date.now), Date.now));
	it('#2', () => assert.equal(store.get('now')(), Date.now()));
});

describe('Multi keys', () => {
	it('#1', () => assert.deepEqual(store.set('shop:cars:folkswagen:colors', ['white', 'black']), ['white', 'black']));
	it('#2', () => assert.deepEqual(store.get('shop:cars:folkswagen:colors'), ['white', 'black']));
	it('#3', () => assert.deepEqual(store.get('shop:cars:folkswagen'), {colors: ['white', 'black']}));
	it('#4', () => assert.deepEqual(store.get('shop:cars'), {folkswagen: {colors: ['white', 'black']}}));
	it('#5', () => assert.deepEqual(store.get('shop'), {cars: {folkswagen: {colors: ['white', 'black']}}}));
});

describe('Undefined & Null & NaN', () => {
	it('#1', () => assert.equal(store.set('undefined', undefined), undefined));
  it('#2', () => assert.equal(store.get('undefined'), undefined));
	it('#3', () => assert.equal(store.set('null', null), null));
  it('#4', () => assert.equal(store.get('null'), null));
	it('#5', () => assert.ok(isNaN(store.set('NaN', NaN))));
  it('#6', () => assert.ok(isNaN(store.get('NaN'))));
});
