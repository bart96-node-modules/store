/*****************************************************************
 * @author Prjakhin Igor <igor96@rambler.ru>
 * https://i96.dev Respect other peoples work
 *
 * @project Store - is an in-memory data storage
 * @version Starting work - 2020-01-05
 *****************************************************************/

'use strict';

const data = new Map();


class Store {
	constructor() {
    // ...
	}


  set(path, value) {
    let keys = this._keys(path);

    if (keys.length === 1) data.set(path, value);
    else {
      let pop = keys.pop();
      let ctn = data.has(keys[0])
        ? data.get(keys[0])
        : data.set(keys[0], Object.create(null)).get(keys[0]);

      for (let key of keys.slice(1)) {
        if (!({}).hasOwnProperty.call(ctn, key)) ctn[key] = Object.create(null);
        ctn = ctn[key];
      }

      ctn[pop] = value;
    }

    return value;
  }


  get(path) {
    let keys = this._keys(path);

    if (keys.length === 1) return data.get(path);
    else {
      if (!data.has(keys[0])) return undefined;

      let ctn = data.get(keys[0]);

      for (let key of keys.slice(1)) {
        if (!({}).hasOwnProperty.call(ctn, key)) return undefined;
        ctn = ctn[key];
      }

      return ctn;
    }
  }


  has(path) {
    let keys = this._keys(path);

    if (keys.length === 1) return data.has(path);
    else {
      if (!data.has(keys[0])) return false;

      let ctn = data.get(keys[0]);

      for (let key of keys.slice(1)) {
        if (!({}).hasOwnProperty.call(ctn, key)) return false;
        ctn = ctn[key];
      }

      return true;
    }
  }


  _show() {
    console.log(data);
  }


  _keys(path) {
    let keys = path.split(':').filter(key => key);
    if (keys.length < 1) throw new Error('Keys not found');
    return keys;
  }

}


const store = new Store();

exports.Store = Store;
store.default = store;
exports.default = store;
module.exports = store;
